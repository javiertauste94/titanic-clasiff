### Estructura del documento

* Módulo_N: Múltiples módulos independientes ente si
    * config: archivos de configuración 
    * data:
        * externar: datos externos para enriquecer
        * interim: datos "a medio procesar"
        * processed: datos ya procesados y limpios
        * raw: datos iniciales
    * logs: archivos de registro .log
    * models_saved: guarda modelos y recursos a usar
    * resources:
        * plots: gráficas 
    * src:
        * internal_tools: herramientas alternativas
        * model: entrenamiento e inferencia
        * preprocess_data: procesamiento y limpieza de datos
            * exploration: exploración de los datos
        * utils: funciones auxiliares
* .gitignore
* README.md
* app.py: aplicación flask
* requirements.txt

Tantos Módulo_N como funcionalidades existan.


### En time_series:
solo importa el notebook de time_series/src/preprocess_data/exploration/Exploración.ipynb