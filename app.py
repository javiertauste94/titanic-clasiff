#!/usr/bin/env python3
# coding: utf-8


# Copyright (C) Solver Machine Learning - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by SolverML <info@solverml.com>, junio 2020
#


""" app.py
"""

from flask import Flask, jsonify, request, abort
import functools
import logging
from werkzeug.exceptions import HTTPException
import traceback
import sys

# from sumrgbchannel.images.sumchannels import cuadrant_aggregate_rgbchannels
# from sumrgbchannel.utils.common import allowed_file
from titanic_classif.src import ml
from titanic_classif.config import config

api_logger = logging.getLogger('apilogger')
app = Flask(__name__)

@app.route("/titanic_classif", methods=['POST'])
def application():

    try:
        if request.method == 'POST':

            files = request.files

            if len(files) < 1:
                api_logger.warning('No data transferred.')
                return jsonify('No data transferred.'), 404

            elif len(files) < config.NUM_FILE_LIMIT:
                file_list = []

                for key, value in files.items():
                    try:
                        if True:  # Comrpobar si datos aportados son válidos
                            # Llamada a la función principal del proyecto
                            ml()  # Usar el módulo titanic_classif, por ejemplo

                            # TODO: el cliente enviaría un @app.route("/titanic_classif/train", methods=['POST']) enviando
                            #  el dataset de entrenamiento para después @app.route("/titanic_classif/test", methods=['POST'])
                            #  predecir(inferencia/test) enviando muestra/s.
                            #  Desarrollar ambas peticiones

                        else:
                            print('Not processed, corrupted data.')

                    except Exception:
                        error = traceback.format_exc()
                        api_logger.warning('Not processed file: {e}'.format(e=error))

                api_logger.info('OK')
                return jsonify('Data processed successfully.'), 200
            else:
                api_logger.warning('Number of files in the request exceeded.')
                return jsonify('Request entity too large: The limit is {lim}'.format(lim=config.NUM_FILE_LIMIT)), 413
        else:
            api_logger.warning('It was not a POST request')
            return jsonify('Only POST admitted'), 404

    except Exception:
        error = traceback.format_exc()
        api_logger.critical('%', error)
        return jsonify('ERROR'), 404


@app.errorhandler(Exception)
def handle_error(e):
    """Global error handler
    """
    code = e.code
    if isinstance(e, HTTPException):
        code = e.code
    url = request.url
    ip = request.remote_addr
    api_logger.error(ip + url + str(code) + str(e))
    return jsonify(str(e)), code


if __name__ == "__main__":
    print(" * Running on https://{host}:{port}/ (Press CTRL+C to quit)".format(host=config.HOST, port=config.PORT))
    sys.stdout.flush()
    app.run(host=config.HOST, port=config.PORT, threaded=True, debug=False)
