#!/usr/bin/env python3
# coding: utf-8


# Copyright (C) Solver Machine Learning - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by SolverML <info@solverml.com>, junio 2020
#


""" __init__.py
"""

import os
import logging
from flask import request
from logging.handlers import TimedRotatingFileHandler
from logging import FileHandler

# Sólo se usan aquí, no necesario poner en config.py
ip_jordi = '185.134.40.19'
ip_solver = '79.148.237.35'


class ContextualFilter(logging.Filter):
    """
        Filter si queremos usar la app en un servidor y que el log muestre más info
    """
    def filter(self, log_record):
        """Filter"""
        log_record.ip = request.remote_addr  # request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
        if log_record.ip == ip_solver:
            log_record.ip = 'solver'
        elif log_record.ip == ip_jordi:
            log_record.ip = 'jordi'
        log_record.method = request.method
        log_record.url = request.path
        log_record.file = ""
        for x in request.files:
            log_record.file += request.files[x].filename + " "
        return True


# formatter personalizado para que info y debug no muestren qué línea del código genera el mensaje
class MyFormatter(logging.Formatter):
    """
        Logger formatter
    """
    # info_fmt = '%(asctime)s - %(levelname)-7s: %(message)s'# log para app normal
    info_fmt = '%(asctime)s - %(ip)s - %(method)s - %(levelname)s: %(message)s - %(file)s'# log para server
    # err_fmt = '%(asctime)s - %(levelname)-7s: %(message)s (%(module)s.%(funcName)s:%(lineno)s)'# log para app normal
    err_fmt = '%(asctime)s - %(ip)s - %(method)s - %(levelname)s: %(message)s - %(file)s (%(module)s.%(funcName)s:%(lineno)s)'# log para server

    def __init__(self, fmt="%(levelno)d: %(msg)s", datefmt=None, style='%'):
        super().__init__(fmt=fmt, datefmt=datefmt, style=style)

    def format(self, record):
        # Save the original format configured by the user
        # when the logger formatter was instantiated
        format_orig = self._style._fmt

        # Replace the original format with one customized by logging level
        if record.levelno in [logging.ERROR, logging.CRITICAL]:
            self._style._fmt = MyFormatter.err_fmt
        else:
            self._style._fmt = MyFormatter.info_fmt

        # Call the original formatter class to do the grunt work
        result = logging.Formatter.format(self, record)

        # Restore the original format configured by the user
        self._style._fmt = format_orig

        return result

# Main logger
main_logger = logging.getLogger('apilogger')
main_logger.setLevel(logging.INFO)
main_logger.propagate = False

my_path = os.path.dirname(__file__)
logs_file = os.path.join(my_path, '../titanic_classif/logs/api.log')

handler = FileHandler(logs_file, encoding='utf-8')
handler.setLevel(logging.INFO)

formatter = MyFormatter(datefmt="%Y-%m-%d %H:%M:%S")
handler.setFormatter(formatter)
main_logger.addHandler(handler)
main_logger.addFilter(ContextualFilter())