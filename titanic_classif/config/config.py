#!/usr/bin/env python3
# coding: utf-8


# Copyright (C) Solver Machine Learning - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by SolverML <info@solverml.com>, junio 2020
#


""" config.py
"""


RND_STATE = 42


# Server connetion _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-__-_-_-_-_-_-_-_-_-_-_-_-_-
HOST = '127.0.0.1'
PORT = 3000
NUM_FILE_LIMIT = 50


# Cleaning and processing data parameters _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
BINS_FARE = 3                            # Number of intervals in Fare (qcut)
BINS_AGE = 5                             # Number of intervals in Age (cut)
LIMIT_AGE = 100.0                        # Upper limit age
FLAG_PARTITION = 0                       # 0: 5CV,
                                         # 1: 10CV,
                                         # 2: 70/30 rand sample


# I/O parameters _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
PERSISTENCE_MODEL = True                 # Save or not the model
IS_TRAINED = True                       # Are there models to predict (and you want to use it)?
MODEL_NAME = '../models_saved/trained_model.pkl'
AUXDATA_NAME = '../models_saved/auxdata_trainfit.pkl'


# Training parameters _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
SKFOLD = 3
CV = 5
TRAIN_VERBOSE = False
SCORING = "f1"
N_JOBS = 1                               # Number of CPU cores used in training
MODEL_SELECTION = 2                      # Select the model: 0: XGBoost,
                                                           # 1: RF,
                                                           # 2: LightGBoost,
                                                           # 3: Probe all


