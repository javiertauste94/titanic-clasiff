#!/usr/bin/env python3
# coding: utf-8


# Copyright (C) Solver Machine Learning - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by SolverML <info@solverml.com>, junio 2020
#


""" common.py
"""

"""
    Funciones de utilidad comunes a cualquier script en src.
"""