#!/usr/bin/env python3
# coding: utf-8


# Copyright (C) Solver Machine Learning - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by SolverML <info@solverml.com>, junio 2020
#


""" processTest.py
"""

import pandas as pd
from titanic_classif.config import config


def clean_test_data(df_test, data_trans):
    """
        Clean the test dataframe: dealing with nulls or misspelled values, aggregating columns or dropping it,
        binarizing countinuous data and hight cardinality data.
        :param df: The original dataframe
        :param dict_config_test: A dictionary containing the intervals used in Fare and Age and the Age aggregation
        :return: The cleaned dataframe and dict_config_test if it's not given
    """

    print("Cleaning test data...")

    df_test_cleaned = data_trans.transform(df_test)

    print("Test data cleaned.")

    return df_test_cleaned
