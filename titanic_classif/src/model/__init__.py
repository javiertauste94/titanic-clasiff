#!/usr/bin/env python3
# coding: utf-8


# Copyright (C) Solver Machine Learning - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by SolverML <info@solverml.com>, junio 2020
#


""" __init__.py
    Patrón de diseño GRASP
    Funcionalidad de este script: ordenar el código
    asignar responsabilidades y ayudar a la legibilidad
    y el mantenimiento futuro.
"""

import sys
import pickle
import numpy as np
import pandas as pd
from datetime import datetime
from titanic_classif.config import config
from titanic_classif.src.preprocess_data import processTrain
from titanic_classif.src.preprocess_data import processTest
from titanic_classif.src.model.trainmodel import train
from titanic_classif.src.model.testmodel import test


class Learner(object):
    """
        Los datos como parámetros, datos, etc no se devuelven en returns
        si no que se guardan en el objeto. El dataframe ya procesado se
        guarda como una variable en esta clase y además se guarda en memoria
        persistente por si se desea testear varias veces sin necesidad de
        entrenar cada vez.
    """

    def __init__(self):
        self.train_df = None
        self.test_df = None

        self.X_train = None
        self.y_train = None
        self.model = None

        self.X_train_clean = np.array([])
        self.X_test_clean = np.array([])
        self.data_transformer = None

    def load_data(self, path, file, is_train=True):
        """
            Dada una ruta, carga los datos en formato pandas dataframe.
            :param path: string, ruta al directorio del conjunto de datos.
            :param file: string, nombre del archivo que contiene los datos.
            :return:
        """
        if is_train:
            self.train_df = pd.read_csv("{p}{f}".format(p=path, f=file))
        else:
            self.test_df = pd.read_csv("{p}{f}".format(p=path, f=file))

    def load_trained_model(self, model_name):
        if model_name:
            self.model = pickle.load(open(config.MODEL_NAME.rsplit(',', 1)[0] + "/" + model_name, 'rb'))
        else:  # Sobreescribir el de por defecto
            self.model = pickle.load(open(config.MODEL_NAME, 'rb'))

        self.data_transformer = pickle.load(open(config.AUXDATA_NAME, 'rb'))
        return None

    def save_data_transformer(self, data_trans):
        pickle.dump(data_trans, open(config.AUXDATA_NAME, 'wb'))
        return

    def save_clean_train_data(self, X_train_clean):
        now = datetime.now()
        date = now.strftime("%d%m%Y_%H%M%S")
        X_train_clean = pd.DataFrame(data=X_train_clean)
        X_train_clean.to_csv('../data/processed/X_train_{date}.csv'.format(date=date), encoding='utf-8')
        return

    def save_clean_test_data(self, X_test_clean):
        now = datetime.now()
        date = now.strftime("%d%m%Y_%H%M%S")
        X_test_clean = pd.DataFrame(data=X_test_clean)
        X_test_clean.to_csv('../data/processed/X_test_{date}.csv'.format(date=date), encoding='utf-8')
        return

    def save_best_model(self, best_model_fit, model_name):
        if config.PERSISTENCE_MODEL:
            try:
                if model_name:
                    model_path = config.MODEL_NAME.rsplit(',', 1)[0]+"/"+model_name
                    pickle.dump(best_model_fit, open(model_path, 'wb'))
                    print("Trained model saved as {}".format(model_path))
                else:
                    pickle.dump(best_model_fit, open(config.MODEL_NAME, 'wb'))
                    print("Trained model saved as {}".format(config.MODEL_NAME))

            except IOError:
                print("Something went wrong saving train model")
        else:
            print("Trained model not saved.")
        return

    def process_data(self, is_train=True):
        """
            Se tratan los datos dependiendo del conjunto que sea.
            Es necesario tratar con antelación el conjunto de
            train para tratar el conjunto de test.
            :param is_train: booleano, True=train, False=test
            :return:
        """
        if is_train:
            if not self.train_df.empty:
                if config.IS_TRAINED:
                    print("Test not processed because already exists a trained model [config.IS_TRAINED].")

                else:
                    self.X_train = self.train_df.drop(['Survived'], axis=1)
                    self.y_train = self.train_df['Survived']

                    self.X_train_clean, self.data_transformer = processTrain.clean_train_data(X=self.X_train)
                    self.save_clean_train_data(self.X_train_clean)
                    self.save_data_transformer(self.data_transformer)
            else:
                print("Train data not loaded yet.")
                sys.exit(0)
        else:
            if not self.test_df.empty and self.data_transformer:  # Test cargado y Train procesado ?
                self.X_test_clean = processTest.clean_test_data(df_test=self.test_df, data_trans=self.data_transformer)
                self.save_clean_test_data(self.X_test_clean)
            else:
                print("Test data not loaded yet or train data not processed.")
                sys.exit(0)

    def train_data(self, model_name=None, path=None, file=None):
        """
            Entrena con los datos del train ya limpios. En caso de que train no haya sido
            limpiado se debe cargar manualmente con path y file.
            :param path: string, ruta al directorio del conjunto de datos.
            :param file: string, nombre del archivo que contiene los datos.
            :return:
        """

        if config.IS_TRAINED:
            try:  # Load resources
                self.load_trained_model(model_name)
                print("Existing model loaded.")

            except IOError:
                print("Something went wrong loading saved model")
                sys.exit(0)
        else:
            if self.X_train_clean.size == 0:
                if path and file:
                    try:
                        self.X_train_clean = pd.read_csv("{p}{f}}".format(p=path, f=file), index_col=0, header=0)
                    except:
                        print("Clean data train data not loaded.")
                else:
                    print("No path/file to locate clean train dataset.")
                    # print("The system can not find the path/file specified.")
                    sys.exit(0)

            self.model, best_model_fit = trainmodel.train(X=self.X_train_clean, y=self.y_train)
            self.save_best_model(best_model_fit, model_name)
        return

    def test_data(self, yTest_name, show_results=False, path=None, file=None, model_name=None):
        """
            Inferencia de los datos según un modelo que, o se ha entrenado o en
            caso de no existir en self. se carga gracias a model_name.
            Para poder inferenciar se necesita el test procesado, si no lo
            tiene en self. lo intenta cargar con path/file.
            :param yTest_name: string, nombre de la columna y del test.
            :param path: string, ruta al directorio del conjunto de datos.
            :param file: string, nombre del archivo que contiene los datos.
            :param model_name: string, nombre del modelo a guardar.
            :return:
        """
        if self.X_test_clean.size == 0:
            if path and file:
                try:
                    self.X_test_clean = pd.read_csv("{p}{f}".format(p=path, f=file), index_col=0, header=0)
                except:
                    print("Clean data test data not loaded.")
            else:
                print("The system can not find the path/file specified.")
                sys.exit(0)

        if config.IS_TRAINED:
            if not self.model:  # si no hay un modelo en self. pero sí que existe en memoria
                try:  # Load resources
                    self.load_trained_model(model_name)
                    print("Existing model loaded.")
                    testmodel.test(self.model, self.X_test_clean, "../data/raw/{ytn}".format(ytn=yTest_name),
                                   show_results)
                except IOError:
                    print("Something went wrong loading saved model.")
                    sys.exit(0)
        else:
            print("No model saved in storage [config.IS_Trained].")
            sys.exit(0)

