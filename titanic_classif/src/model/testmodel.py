#!/usr/bin/env python3
# coding: utf-8


# Copyright (C) Solver Machine Learning - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by SolverML <info@solverml.com>, junio 2020
#


""" testmodel.py
"""

from sklearn import metrics
import matplotlib.pyplot as plt
import pandas as pd


def showperformance(model, y_true, y_pred, X_test):
    class_labels = ['Dead', 'Survived']

    class_report = metrics.classification_report(y_true, y_pred, target_names=class_labels)

    df_confm = pd.DataFrame(data=metrics.confusion_matrix(y_true, y_pred),
                            index=["dead_pred", "survived_pred"],
                            columns=["dead_true", "survived_true"])

    df_confm_norm = pd.DataFrame(data=metrics.confusion_matrix(y_true, y_pred, normalize='true').round(2),
                                 index=["dead_pred", "survived_pred"],
                                 columns=["dead_true", "survived_true"])

    metrics.plot_roc_curve(model, X_test, y_true)

    print("Prediction performance: ")
    print("Accuracy (%hit): ", round(metrics.accuracy_score(y_true, y_pred), 2))
    print("ROC-AUC: ", round(metrics.roc_auc_score(y_true, y_pred), 2))
    print("Cohen-Kappa: ", round(metrics.cohen_kappa_score(y_true, y_pred), 2))

    print(class_report)
    print(df_confm)
    print(df_confm_norm)
    plt.show()


def test(model, X_test, data_path, show_results):
    """
        Test de model
        :param model: The model trained by trainmodel.py
        :param df: The dataframe without the dependent variable
        :return: The dataframe completed with the prediction
    """
    print("Predicting data...")
    y_pred = model.predict(X_test)

    y_true = pd.read_csv(data_path, index_col=0)
    if show_results:
        showperformance(model, y_true, y_pred, X_test)