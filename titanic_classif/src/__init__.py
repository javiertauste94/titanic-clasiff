#!/usr/bin/env python3
# coding: utf-8


# Copyright (C) Solver Machine Learning - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by SolverML <info@solverml.com>, junio 2020
#


""" __init__.py
    Script para pruebas en src, sin llamadas GET/POST
"""

from titanic_classif.src.model import Learner

def ml():

    # # Opción 1: entrenar por primera vez y predecir en la misma ejecución
    # learn = Learner()
    #
    # learn.load_data(path='../data/raw/', file='train.csv', is_train=True)       # Carga el train
    # learn.process_data(is_train=True)                                           # Procesa el train
    # learn.train_data()                                                          # Entrena
    #
    # learn.load_data(path='../data/raw/', file='test.csv', is_train=False)       # Carga el test
    # learn.process_data(is_train=False)                                          # Procesa el test
    # learn.test_data(yTest_name='gender_submission.csv', show_results=False)     # Testea


    # Opción 2: tener un modelo ya entrenado y cargarlo para predecir
    learn = Learner()
    learn.test_data(yTest_name='gender_submission.csv',
                    show_results=True,
                    path='../data/processed/',
                    file='X_test_17062020_132544.csv',
                    model_name=None)


if __name__ == '__main__':
    ml()



