#!/usr/bin/env python3
# coding: utf-8


# Copyright (C) Solver Machine Learning - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by SolverML <info@solverml.com>, junio 2020
#


""" processTrain.py
"""

from sklearn.preprocessing import OneHotEncoder, MinMaxScaler, FunctionTransformer
from sklearn.compose import make_column_transformer
from sklearn.pipeline import make_pipeline
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import (SimpleImputer, IterativeImputer)
from sklearn.preprocessing import KBinsDiscretizer
import pickle
import numpy as np
from titanic_classif.config import config
from datetime import datetime

import pandas as pd
import sys





def process_names(df):
    df['Title'] = df['Name'].str.extract(r'([A-Za-z]+)\.', expand=False)

    df['Title'] = df['Title'].replace({'Mlle': 'Miss', 'Ms': 'Miss', 'Mme': 'Mrs'})
    df['Title'] = df['Title'].replace(['Lady', 'Countess', 'Capt', 'Col', 'Don', 'Dr',
                                       'Major', 'Rev', 'Sir', 'Jonkheer', 'Dona'], 'Rare')

    # Even though it's on to_be_removed, the column 'Name' loses its name and can't be dropped
    df = df.drop(['Name'], axis=1)
    return df


def process_is_alone(df):
    df['isAlone'] = df.apply(lambda x: 0 if (x['SibSp'] == 0.0) & (x['Parch'] == 0.0) else 1, axis=1)
    return df


def clean_train_data(X):
    """
        Clean the train dataframe: dealing with nulls or misspelled
        values, aggregating columns or dropping it, binarizing
        countinuous data and hight cardinality data.
        :param df: The original dataframe
        :return: The cleaned dataframe and dict_config_test if it's not given
    """

    print("Cleaning train data...")

    numeric_bin_features = ['Age', 'Fare']
    numeric_bin_transformer = make_pipeline(
        IterativeImputer(missing_values=np.nan,
                         random_state=42,
                         n_nearest_features=None,
                         sample_posterior=True,
                         add_indicator=True),
        KBinsDiscretizer(n_bins=2,
                         encode='ordinal',
                         strategy='uniform'),  # quantile, underpopulated bins
        MinMaxScaler()
    )

    numeric_features = ['Parch', 'SibSp']
    numeric_transformer = make_pipeline(
        FunctionTransformer(process_is_alone),
        # add_indicator=True when missingness could be useful to your model
        SimpleImputer(strategy='median', add_indicator=True),
        MinMaxScaler())

    text_features = ['Name']
    text_transformer = make_pipeline(
        FunctionTransformer(process_names),  # kw_args=dict(decimals=3)
        SimpleImputer(strategy='constant', fill_value='NoName'),
        OneHotEncoder())

    categorical_features = ['Embarked', 'Sex', 'Pclass']
    categorical_transformer = make_pipeline(
        SimpleImputer(strategy='most_frequent'),
        OneHotEncoder())

    # to_be_removed = ['Name', 'Cabin', 'Ticket', 'PassengerId', 'Title']
    preprocessor = make_column_transformer(
        (text_transformer, text_features),
        (numeric_bin_transformer, numeric_bin_features),
        (numeric_transformer, numeric_features),
        (categorical_transformer, categorical_features),
        # ('drop', to_be_removed),
        remainder='drop')  # i.e. ['Title'] must be removed

    dataprocessor = preprocessor.fit(X)
    X_processed = preprocessor.transform(X)

    # pickle.dump(dataprocessor, open(config.AUXDATA_NAME, 'wb'))


    print("Train data cleaned.")
    return X_processed, dataprocessor

