#!/usr/bin/env python3
# coding: utf-8


# Copyright (C) Solver Machine Learning - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by SolverML <info@solverml.com>, junio 2020
#


""" __init__.py
    Patrón de diseño GRASP.
    Agrupa y delega responsabilidades, organiza el código.
"""

import sys
import pickle
import numpy as np
import pandas as pd
from datetime import datetime

from time_series.config import config
from time_series.src.preprocess_data import processTrain
from time_series.src.preprocess_data import processTest
from time_series.src.model.trainmodel import train
from time_series.src.model.testmodel import test


class Forecaster(object):
    """
        Clase que pronostica una serie temporal.
        Las variables y datos se tratan como variables de clase,
        esto es para testear sin tener que entrenar cada vez.
        Hay funciones para guardar en memoria permanente.
    """

    def __init__(self):
        self.df = None
        self.model = None
        self.data_transformer = None

    def load_data(self, path, file):
        """
            Carga los datos en formato pandas dataframe dada una ruta.
            :param path: string, ruta al directorio del conjunto de datos.
            :param file: string, nombre del archivo que contiene los datos.
            :return:
        """
        path = "{p}{f}".format(p=path, f=file)
        self.df = pd.read_csv(path,
                              na_values=".",
                              parse_dates=True,
                              header=0,
                              index_col=0,
                              infer_datetime_format=True
                              ).dropna()

    def process_data(self, is_train=True):
        """
            Limpia y transforma los datos.
            El proceso de transformación se guarda en una variable
            scikit para ser empleado en conjuntos de test.
            :param is_train: bool. True=train, False=test
            :return:
        """
        print( self.df )

        # 0.  Exploración previa en /preprocess_data/exploration
        # 1.  ¿Está la fecha como el índice del dataframe?
        # 2.  ¿Es necesario agrupar distintas columnas (MONTH, DAY)?
        # 3.  ¿Es necesario separar la fecha en otras (DAY_NAME, YEAR, etc)?
        # 4.  ¿Es necesario eliminar columnas innecesarias?
        # 5.  ¿Es necesario oversamplear/undersamplear ( weekends, festivos, etc)?
        # 6.  ¿Es necesario agrupar las muestras en semanas, meses o años?
        # 7.  ¿Hay valores nulos o codificados como tal?
        # 8.  ¿Hay texto mal formateado?
        # 9.  ¿Hay valores imposibles según el contexto (AGE<0)?
        # 10. ¿Es necesario completar los datos (vacaciones, promociones, etc)?
        # 11. ¿Hay outliers y es necesario tratarlos?

        return

    def train_data(self, model_name=None, path=None, file=None):
        """
            Entrena con los datos del train ya limpios.
            :param path: string, ruta al directorio del conjunto de datos.
            :param file: string, nombre del archivo que contiene los datos.
            :return:
        """
        return

    def save_data_transformer(self, data_trans):
        pickle.dump(data_trans, open(config.AUXDATA_NAME, 'wb'))
        return

    def save_clean_data(self, X_train_clean):
        now = datetime.now()
        date = now.strftime("%d%m%Y_%H%M%S")
        X_train_clean = pd.DataFrame(data=X_train_clean)
        X_train_clean.to_csv('../data/processed/clean_data_forecast_{date}.csv'.format(date=date), encoding='utf-8')
        return


