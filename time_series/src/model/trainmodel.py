#!/usr/bin/env python3
# coding: utf-8


# Copyright (C) Solver Machine Learning - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by SolverML <info@solverml.com>, junio 2020
#


""" trainmodel.py
"""

import pickle
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV, RepeatedStratifiedKFold #, RandomizedSearchCV
from sklearn.ensemble import RandomForestClassifier
# from sklearn import preprocessing
# from sklearn.feature_selection import SelectFromModel

from xgboost import XGBClassifier, plot_importance
from lightgbm import LGBMClassifier

from matplotlib import pyplot
from titanic_classif.config import config




class MyXGBClassifier(XGBClassifier):
    """
    Define custom class to fix bug in xgboost 1.0.2
    """
    @property
    def coef_(self):
        return None


def plot_importance_features(best_estimator, name_best_estimator, X_columns):
    """
    Plot a bar graph with the importance of the data features
    :param best_estimator: The best model trained
    :param name_best_estimator: The name of the best model trained
    :param X_columns: A list of names of every feature
    :return: None
    """
    if name_best_estimator in ("XGBClassifier", "MyXGBClassifier"):
        booster = best_estimator.get_booster()

        # ['weight', 'gain', 'cover', 'total_gain', 'total_cover']
        importance = booster.get_score(importance_type="weight")
        importance = dict(zip(X_columns, list(importance.values())))
        importance = {k: round(v, 2) for k, v in importance.items()}

        plot_importance(importance)
        pyplot.show()

    else:
        importance = dict(zip(X_columns, best_estimator.feature_importances_))
        importance = {k: round(v, 2) for k, v in importance.items()}
        plot_importance(importance)
        pyplot.show()


def train(X, y):
    """
    Training a model with a processed dataframe which classifies a binary variable 'target'
    :param df: The dataframe with will train the model
    :param target: The string name of the dependent variable
    :return: The trained model
    """

    print("Training model...")
    # X = df.loc[:, df.columns != target]
    # y = df.loc[:, target]

    clf = MyXGBClassifier()
    # std_scaler = preprocessing.StandardScaler()
    # selector = SelectFromModel(clf)
    # pipe_params = [('std_scaler', std_scaler),
    #               ('select', selector),
    #               ('clf', clf)] # Too complex fro this dataset
    pipe_params = [('clf', clf)]
    pipe = Pipeline(pipe_params)

    param_grid = [{
        # 'clf__min_child_weight': [1],
        'clf__objective': ["binary:logistic"],
        'clf__gamma': [0.01, 0.1, 0.3, 0.4, 0.5],
        'clf__n_estimators': [50, 75, 100, 120],
        # 'clf__subsample': [1.0],
        'clf__max_depth': [2, 3, 4, 5],
        'clf__random_state': [config.RND_STATE],
        'clf__learning_rate': [0.0, 0.01, 0.05, 0.1],
        'clf__silent': [config.TRAIN_VERBOSE],
        # 'select__estimator__threshold': [i / 10.0 for i in range(0, 10)],
    }, {
        'clf': [RandomForestClassifier()],
        # 'clf__min_samples_leaf': [1],
        'clf__max_depth': [2, 3, 4, 5],
        'clf__n_estimators': [50, 75, 100, 120],
        'clf__random_state': [config.RND_STATE],
        # 'clf__reduce_dim__n_components': [i / 10.0 for i in range(5, 10)], # ??
        # 'select__estimator__threshold': [i / 10.0 for i in range(0, 10)]
    }, {
        # This is a toy parameters
        'clf': [LGBMClassifier()],
        'clf__num_leaves': [3],
        'clf__max_depth': [2],
        'clf__n_estimators': [120],
        'clf__random_state': [config.RND_STATE],
        'clf__silent': [not config.TRAIN_VERBOSE],
        'clf__learning_rate': [0.05],

        # 'clf': [LGBMClassifier()],
        # 'clf__num_leaves': range(3, 20, 2),
        # 'clf__max_depth': [2, 3, 4, 5],
        # 'clf__n_estimators': [50, 75, 100, 120],
        # 'clf__random_state': [config.RND_STATE],
        # 'clf__silent': [not config.TRAIN_VERBOSE],
        # 'clf__learning_rate': [ 0.01, 0.05, 0.1, 0.2],
        # # 'select__estimator__threshold': [i / 10.0 for i in range(0, 10)]
    }]
    param_grid = param_grid[config.MODEL_SELECTION] if config.MODEL_SELECTION != 3 else param_grid

    cv = RepeatedStratifiedKFold(n_splits=config.CV, n_repeats=config.SKFOLD, random_state=config.RND_STATE)
    grid = GridSearchCV(pipe, param_grid, scoring=config.SCORING, n_jobs=config.N_JOBS, cv=cv,
                        verbose=config.TRAIN_VERBOSE, refit=True)  # RandomizedSearchCV
    best_model_fit = grid.fit(X, y)

    best_estimator = grid.best_estimator_.named_steps["clf"]
    name_best_estimator = best_estimator.__class__.__name__
    print("Best training score %s: %0.3f using %s" % (config.SCORING, grid.best_score_, name_best_estimator))

    # plot_importance_features(best_estimator, name_best_estimator, X_columns)

    return best_estimator, best_model_fit