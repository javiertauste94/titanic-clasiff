#!/usr/bin/env python3
# coding: utf-8


# Copyright (C) Solver Machine Learning - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by SolverML <info@solverml.com>, junio 2020
#


""" __init__.py
    Script para pruebas en src, sin llamadas GET/POST
"""

from time_series.src.model import Forecaster

if __name__ == '__main__':
    learn = Forecaster()
    # Carga el train
    learn.load_data(path='../data/raw/', file='AirPassengers.csv')
    # Limpia y transforma el train
    learn.process_data()
    ## Entrena con el train
    # learn.train_data()



